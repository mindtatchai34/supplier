<div class="c-sidebar-brand d-lg-down-none">
    <img src="{{ asset('image/logo.gif') }}" class="logo-nation" alt="">

    <svg class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
        <use xlink:href="assets/brand/coreui.svg#signet"></use>
    </svg>
</div>
<ul class="c-sidebar-nav">
    <li class="c-sidebar-nav-title"><strong>Login : </strong> {{ Auth::user()->name }}</li>

    <li class="c-sidebar-nav-item"><a
            class="c-sidebar-nav-link {{ Request::segment(1) == "dashboard" ? "c-active" : "" }}"
            href="{{ route('admin.product.index') }}" >
            <i class="fab fa-product-hunt"></i>
            <span class="ml-4">จัดการ <strong>ORDER</strong> </span>
        </a>
    </li>
    <li class="c-sidebar-nav-item"><a
                class="c-sidebar-nav-link"
                href="{{ route('auth.logout') }}" >
            <i class="fas fa-sign-out-alt"></i>
            <span class="ml-4">ออกจากระบบ </span>
        </a>
    </li>

</ul>
