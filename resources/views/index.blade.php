@extends('Layouts.user')
@section('contents')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="card pr-3 pl-3">

                <div class="row mt-3 ml-2">
                    <blockquote class="blockquote ml-2">
                        <p><em>หมายเหตุ</em></p>
                        <ul>
                            <li>หากขึ้นสถานะ "สินค้ารอตรวจสอบ" ทางเราจะจัดส่งให้ลูกค้าภายใน 2-3 วัน ไม่รวมวันอาทิตย์ค่ะ (กรณีชำระเงินแบบรอบเดียว)</li>
                            <li>หากเป็นลูกค้าที่ชำระเงิน 2 รอบ ให้รอการตอบกลับจากทางแอดมินเพื่อชำระเงินรอบที่ 2 ค่ะ</li>
                            <li>หากสถานะแจ้งว่า "สินค้ารอตรวจสอบ" ลูกค้าสามารถเข้ามาเช็คเลขพัสดุในไทยเพื่อส่งไปบ้านลูกค้าได้ ภายใน 2-3 วัน ไม่รวมวันอาทิตย์ค่ะ</li>
                            <li>ลูกค้าสามารถแจ้งเปลี่ยนที่อยู่จัดส่งได้เมื่อขึ้นสถานะ "กำลังมาไทย" หากต้องการเปลี่ยนที่อยู่จัดส่ง</li>
                        </ul>
                    </blockquote>

                </div>
                <div class="card-header">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-6">
                                <b>กรอกเลขคำสั่งซื้อของคุณ</b>
                                <input type="text" class="form-control mt-1" name="code" placeholder="101102" value="{{ empty($_GET['code']) ? null : $_GET['code'] }}">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-info mt-4" value="ค้นหา">
                            </div>
                        </div>
                    </form>
                </div>
                <div style="overflow-x:auto;">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">เลขที่สั่งซื้อ</th>
                            <th scope="col">รูปภาพ</th>
                            <th scope="col">ชื่อสินค้า</th>
                            <th scope="col">Facebook ลูกค้า</th>
                            <th scope="col">สถานะสินค้า</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($product))
                            @foreach($product as $products)
                                <tr>
                                    <td>{{ $products->orderCode }}</td>
                                    <td>
                                        <img src="{{ asset('thumbnail/'.$products->imgName) }}" alt="">
                                    </td>
                                    <td>{{ $products->name }}</td>
                                    <td><a href="{{ $products->fbLink }}">{{ $products->fbName }}</a></td>
                                    <td>
                                        <span class="{{ $products->status->class }}">{{ $products->status->statusName }}</span>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" style="text-align: center">กรอกคำสั่งซื้อของคุณ</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>

                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        @if(!empty($product))
                            {{ $product->links() }}
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection