@extends('Layouts.main')
@section('contents')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="card pr-3 pl-3">
                <div class="card-header">
                    <div class="row">

                        <div class="col-md-7">
                            <form action="" method="GET">
                                <div class="row">
                                    <div class="col-md-6">
                                        <b>กรอกเลขคำสั่งซื้อของคุณ</b>
                                        <input type="text" class="form-control mt-1" name="code" placeholder="101102" value="{{ empty($_GET['code']) ? null : $_GET['code'] }}">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="submit" class="btn btn-info mt-4" value="ค้นหา">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4 text-right mt-3">
                            <a href="{{ route('admin.product.create') }}" class="btn btn-info">เพิ่ม Order</a>
                        </div>
                    </div>
                </div>
                <div style="overflow-x:auto;">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">เลขที่สั่งซื้อ</th>
                            <th scope="col">รูปภาพ</th>
                            <th scope="col">ชื่อสินค้า</th>
                            <th scope="col">Facebook ลูกค้า</th>
                            <th scope="col">สถานะสินค้า</th>
                            <th scope="col">วันที่เพิ่มข้อมูล</th>
                            <th scope="col">จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($product as $products)
                            <tr>
                                <td>{{ $products->orderCode }}</td>
                                <td>
                                    <img src="{{ asset('thumbnail/'.$products->imgName) }}" alt="">
                                </td>
                                <td>{{ $products->name }}</td>
                                <td><a href="{{ $products->fbLink }}">{{ $products->fbName }}</a></td>
                                <td><span class="{{ $products->status->class }}">{{ $products->status->statusName }}</span>
                                <td><span class="badge bg-secondary">{{ formatDateThat($products->createAt) }}</span></td>
                                <td>
                                    <a href="{{route('admin.product.edit',$products->id)}}" class="btn btn-warning">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <button class="btn btn-danger btn-delete" data-id="{{ $products->id }}">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        {{ $product->links() }}
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    @include('product.modal')
@endsection
@section('script')
    <script>
        $('.btn-delete').click(function() {
            let id = $(this).attr('data-id');
            document.getElementById("formdelete").action = '{{ url("admin/product") }}/' + id;
            $('#deleteModal ').modal('show')
        });
    </script>
@endsection