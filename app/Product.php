<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'orderCode', 'name', 'imgName', 'statusId', 'address',
        'cusName','fbName', 'fbLink',
        'createAt', 'updateAt'
    ];

    public function Status() {
        return $this->hasOne('App\Status','id', 'statusId');
    }
}
