<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request) {

        $codeKeyword = $request->input('code',null);
        $product = [];
        if($codeKeyword != null) {
            $product = Product::with('Status')->orderBy('id', 'desc')->where('orderCode',$codeKeyword)->paginate(9);
        }

        return view('index', compact('product'));
    }
}
